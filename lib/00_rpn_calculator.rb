class RPNCalculator
  # TODO: your code goes here!

  def initialize
    @array = []
  end

  def push(n)
    @array << n
  end

  def plus
    do_op(:+)
  end

  def minus
    do_op(:-)
  end

  def divide
    do_op(:/)
  end

  def times
    do_op(:*)
  end

  def tokens(string)
    token = string.split
    result = []

    token.each do |element|
      if "+-*/".include?(element)
        result << element.to_sym
      else
        result << element.to_i
      end
    end

    result
  end

  def evaluate(string)
    token = tokens(string)

    token.each do |element|
      if element.is_a? Integer
        @array << element
      else
        do_op(element)
      end
    end

    value
  end

  def value
    @array.last
  end

  private

  def do_op(operation)
    raise "calculator is empty" if @array.size < 2

    second_num = @array.pop
    first_num = @array.pop

    case operation
    when :+
      @array << first_num + second_num
    when :-
      @array << first_num - second_num
    when :*
      @array << first_num * second_num
    when :/
      @array << first_num.fdiv(second_num)
    else
      raise "No good!"
    end
  end
end
